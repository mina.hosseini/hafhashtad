package org.themoviedb.celebrities.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController


fun<T> Fragment.observe(liveData: LiveData<T>?, observer: ((T)->Unit)) {
    liveData?.observe(viewLifecycleOwner, Observer{ observer(it) })
}

fun<T> Fragment.observeNullable(liveData: LiveData<T>?, observer: ((T?)->Unit)) {
    liveData?.observe(viewLifecycleOwner, Observer{ observer(it) })
}

inline fun Fragment.withFragmentManager(crossinline action: (FragmentManager)->Unit) {
    if (isAdded){
        action(parentFragmentManager)
    }
}

fun Fragment.navigate(destination: NavDirections, options: NavOptions? = null) = with(findNavController()) {
    currentDestination?.getAction(destination.actionId)
            ?.let { navigate(destination, options) }
}

fun Fragment.navigateUp() = with(findNavController()) {
    navigateUp()
}

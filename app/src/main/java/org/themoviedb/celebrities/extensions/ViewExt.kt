package org.themoviedb.celebrities.extensions

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.DisplayMetrics
import android.view.*
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.minus
import androidx.core.graphics.plus
import androidx.core.view.*
import com.google.android.material.tabs.TabLayout
import org.themoviedb.celebrities.R
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.properties.ObservableProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

val View.isRTL get() = ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_RTL

fun View.observeGlobalLayoutOnce(observer: ()->Unit) =
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        override fun onGlobalLayout() {
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewTreeObserver.removeOnGlobalLayoutListener(this)
                } else {
                    viewTreeObserver.removeGlobalOnLayoutListener(this)
                }
                observer()
            } catch (ignored: Exception) {
            }
        }
    })


@JvmOverloads
fun View.bitmap(measureAndLayout: Boolean = true) : Bitmap{
    if(measureAndLayout){
        measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        layout(0, 0, measuredWidth, measuredHeight);
    }
    val canvasBitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
    val canvas = Canvas(canvasBitmap)
//    val bgDrawable: Drawable? = background
//    if (bgDrawable != null) bgDrawable.draw(canvas) else canvas.drawColor(Color.WHITE)
//    canvas.save()
    draw(canvas)
//    canvas.restore()
    return canvasBitmap
}


fun Context.screenshotFile(filename: String): File {
    val cacheFile = File(cacheDir, "screenshots/$filename")
    cacheFile.parentFile?.mkdirs()
    return cacheFile
}

fun Bitmap.saveScreenshot(context: Context, filename: String){
    try {
        FileOutputStream(context.screenshotFile(filename)).use { out ->
            compress(Bitmap.CompressFormat.PNG, 100, out)
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
}


val View.locationInWindow : Point get() {
    val ans = intArrayOf(0,0)
    getLocationInWindow(ans)
    return Point(ans[0], ans[1])
}
val View.locationOnScreen : Point get() {
    val ans = intArrayOf(0,0)
    getLocationOnScreen(ans)
    return Point(ans[0], ans[1])
}

val View.rectOnScreen : Rect get() {
    val start = locationOnScreen
    return Rect(start.x, start.y, start.x + measuredWidth, start.y + measuredHeight)
}

inline fun <T> View.invalidator(initialValue: T, crossinline onChange: (property: KProperty<*>, oldValue: T, newValue: T) -> Unit = {_,_,_ -> Unit}):
        ReadWriteProperty<Any?, T> =
        object : ObservableProperty<T>(initialValue) {
            override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) {
                onChange(property, oldValue, newValue)
                invalidate()
            }
        }
inline fun <T> View.layoutChanger(initialValue: T, crossinline onChange: (property: KProperty<*>, oldValue: T, newValue: T) -> Unit = {_,_,_ -> Unit}):
        ReadWriteProperty<Any?, T> =
        object : ObservableProperty<T>(initialValue) {
            override fun afterChange(property: KProperty<*>, oldValue: T, newValue: T) {
                onChange(property, oldValue, newValue)
                requestLayout()
            }
        }

val View.pixelScaleFactor: Float get(){
    return (resources?.displayMetrics?.densityDpi?.toFloat() ?: return 1f) / DisplayMetrics.DENSITY_DEFAULT
}
fun View.dp(dp: Float): Int {
    return (dp * pixelScaleFactor).toInt()
}

fun View.drawableCompat(@DrawableRes drawable: Int?) : Drawable?{
    drawable ?: return null
    return ContextCompat.getDrawable(context , drawable)
}
@ColorInt fun View.colorCompat(@ColorRes color: Int?) : Int{
    color ?: return 0
    return ContextCompat.getColor(context, color)
}

fun CharSequence.getTimeNeededToRead(): Int {
    // Readable words per minute
    val wordPerMinutes = 180
    val minuteInMillis = 60 * 1000

    // Standardized number of chars in calculable word
    val wordLength = 5
    val wordCount = this.length / wordLength
    val readTime = (wordCount / wordPerMinutes) * minuteInMillis

    // Milliseconds before user starts reading the notification
    val startDelay = 1500

    // Extra time
    val endDelay = 1300

    return startDelay + readTime + endDelay
}


val View.inflater: LayoutInflater get() = LayoutInflater.from(context)

fun View.removeFromParent(){
    try {
        (parent as? ViewGroup)?.removeView(this)
    }catch (e: Exception){
//        Events.App.Catches(e).report()
        e.printStackTrace()
    }
}


fun ViewGroup.isMyChild(child: View) : Boolean {
    if(child.parent === this) return true
    return (child.parent as? View)?.let {
        isMyChild(it)
    } ?: false
}

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    }
}

fun View.setMarginTop(newMarginTop: Int) {
    val menuLayoutParams = this.layoutParams as? ViewGroup.MarginLayoutParams ?: return
    menuLayoutParams.apply {
        setMargins(marginLeft, newMarginTop, marginRight, marginBottom)
    }
    this.layoutParams = menuLayoutParams
}

public inline fun <reified R> ViewGroup.childrenWithType(): Sequence<R> {
    @Suppress("UNCHECKED_CAST")
    return children.filter { it is R } as Sequence<R>
}

inline fun TabLayout.changeTabsFont(){
    val vg = this.getChildAt(0) as ViewGroup
    val tabsCount = vg.childCount
    for (j in 0 until tabsCount) {
        val vgTab = vg.getChildAt(j) as ViewGroup
        val tabChildrenCount = vgTab.childCount
        for (i in 0 until tabChildrenCount) {
            val tabViewChild = vgTab.getChildAt(i)
            if (tabViewChild is TextView) {
                tabViewChild.typeface = ResourcesCompat.getFont(vg.context, R.font.iran_sans_medium)
            }
        }
    }
}


fun parseGravity(str: String): Int {
    var gravityAns = 0
    str.splitToSequence("|").forEach {  singleStr ->
        val g = when(singleStr) {
            "end" -> Gravity.END
            "center_vertical" -> Gravity.CENTER_VERTICAL
            "center" -> Gravity.CENTER
            "start" -> Gravity.START
            "bottom" -> Gravity.BOTTOM
            "top" -> Gravity.TOP
            "center_horizontal" -> Gravity.CENTER_HORIZONTAL
            "right" -> Gravity.RIGHT
            "clip_horizontal" -> Gravity.CLIP_HORIZONTAL
            "clip_vertical" -> Gravity.CLIP_VERTICAL
            "fill" -> Gravity.FILL
            "fill_horizontal" -> Gravity.FILL_HORIZONTAL
            "fill_vertical" -> Gravity.FILL_VERTICAL
            "left" -> Gravity.LEFT
            else -> 0
        }
        gravityAns = gravityAns or g
    }
    return gravityAns
}
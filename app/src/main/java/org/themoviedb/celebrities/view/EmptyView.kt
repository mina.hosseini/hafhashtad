package org.themoviedb.celebrities.view

import android.content.Context
import android.util.AttributeSet
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.databinding.ViewEmptyBinding

class EmptyView : BaseCustomView<ViewEmptyBinding> {

    override fun layout(): Int = R.layout.view_empty

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        init(context, attrs)
    }

    fun init(context: Context, attrs: AttributeSet?) {}

    fun setMessage(message: String?) {
        binding.message.text = message ?: ""
    }

}
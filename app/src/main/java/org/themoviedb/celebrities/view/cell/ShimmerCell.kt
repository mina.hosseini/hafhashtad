package org.themoviedb.celebrities.view.cell

import android.content.Context
import android.util.AttributeSet
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.adapter.GenericAdapterView
import org.themoviedb.celebrities.databinding.CellShimmerBinding
import org.themoviedb.celebrities.view.BaseCustomView

class ShimmerCell : BaseCustomView<CellShimmerBinding>, GenericAdapterView<Int> {

    override fun layout(): Int {
        return R.layout.cell_shimmer
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    )

    override fun onBind(model: Int, position: Int, extraObject: Any?) {
        binding.iv.setImageResource(model)

        val p = if(extraObject is IntArray && extraObject.size == 4)
            extraObject
        else
            intArrayOf(0,0,0,0)
        binding.iv.setPadding(p[0], p[1], p[2], p[3])
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if(parent?.javaClass?.name?.contains("ViewPager2") == true) {
            layoutParams.width = LayoutParams.MATCH_PARENT
            layoutParams.height = LayoutParams.MATCH_PARENT
        }
    }
}
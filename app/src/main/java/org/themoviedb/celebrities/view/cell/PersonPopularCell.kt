package org.themoviedb.celebrities.view.cell

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.adapter.GenericAdapterView
import org.themoviedb.celebrities.app.Constants
import org.themoviedb.celebrities.databinding.CellPersonPopularBinding
import org.themoviedb.celebrities.model.response.Person
import org.themoviedb.celebrities.view.BaseCustomView

@SuppressLint("ViewConstructor")
class PersonPopularCell : BaseCustomView<CellPersonPopularBinding>, GenericAdapterView<Person> {

    private var onItemClick: ((item: Person) -> Unit)? = null

    override fun layout(): Int = R.layout.cell_person_popular

    constructor(context: Context) : super(context)

    constructor(
            context: Context,
            onItemClick: (freewayToll: Person) -> Unit,
    ) : super(context) {
        this.onItemClick = onItemClick
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    )

    fun onClick() {
        binding.person?.let {
            onItemClick?.invoke(it)
        }
    }

    override fun onBind(model: Person, position: Int, extraObject: Any?) {
        binding.person = model
    }


}
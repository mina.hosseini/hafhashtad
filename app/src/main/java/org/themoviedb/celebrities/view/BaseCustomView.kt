package org.themoviedb.celebrities.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import org.themoviedb.celebrities.extensions.setView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseCustomView<B : ViewDataBinding> @JvmOverloads constructor(
    context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr), CoroutineScope {

    @LayoutRes abstract fun layout(): Int

    lateinit var binding: B
    private var jobField: Job? = null
    private val job: Job
        get(){
            if(jobField?.isActive != true ){
                jobField?.cancel()
                jobField = Job()
            }
            return jobField!!
        }
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    init {
        this.inflate(attrs)
    }

    @CallSuper
    protected open fun inflate(attrs: AttributeSet?) {
        if (isInEditMode) {
            LayoutInflater.from(context).inflate(layout(), this, true)
        }
        else{
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), layout(), this, true)
            binding.setView(this)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        registerObservers()
    }
    override fun onDetachedFromWindow() {
        observeHistory.forEach {
            it.first.removeObserver(it.second)
        }
        super.onDetachedFromWindow()
        jobField?.cancel()
    }

    open fun registerObservers(){}
    private val observeHistory = arrayListOf<Pair<LiveData<*>, (Any)->Unit>>()

}
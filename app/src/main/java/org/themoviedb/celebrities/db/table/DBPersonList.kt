package org.themoviedb.celebrities.db.table

import android.os.Parcelable
import androidx.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.themoviedb.celebrities.model.response.Person
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "persons")
@Parcelize
data class DBPersonList(

    @PrimaryKey(autoGenerate = true) val page: Int = 0,

    @TypeConverters(Converter::class)
    @ColumnInfo(name = "personList") var personList: List<Person>? = null

) : Parcelable


class Converter {
    @TypeConverter
    fun toPerson(json: String): List<Person> {
        val type = object : TypeToken<List<Person>>() {}.type
        return Gson().fromJson(json, type)
    }

    @TypeConverter
    fun toJson(person: List<Person>): String {
        val type = object: TypeToken<List<Person>>() {}.type
        return Gson().toJson(person, type)
    }
}
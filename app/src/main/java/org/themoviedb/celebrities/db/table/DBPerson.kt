package org.themoviedb.celebrities.db.table

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "person")
@Parcelize
data class DBPerson(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    @ColumnInfo(name = "name") var name: String? = "",
    @ColumnInfo(name = "biography") var biography: String? = "",
    @ColumnInfo(name = "birthday") var birthday: String? = "",
    @ColumnInfo(name = "knownForDepartment") var knownForDepartment: String? = "",
    @ColumnInfo(name = "placeOfBirth") var placeOfBirth: String? = "",
    @ColumnInfo(name = "profilePath") var profilePath: String? = "",
) : Parcelable
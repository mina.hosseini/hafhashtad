package org.themoviedb.celebrities.db

import android.content.Context
import androidx.room.*
import org.themoviedb.celebrities.db.dao.PersonDao
import org.themoviedb.celebrities.db.dao.PersonListDao
import org.themoviedb.celebrities.db.table.DBPersonList
import org.themoviedb.celebrities.db.table.Converter
import org.themoviedb.celebrities.db.table.DBPerson

@Database(entities = arrayOf(DBPersonList::class , DBPerson::class), version = 1, exportSchema = false)
@TypeConverters(Converter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun personListDao(): PersonListDao
    abstract fun personDao(): PersonDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}

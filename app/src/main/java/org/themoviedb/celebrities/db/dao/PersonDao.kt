package org.themoviedb.celebrities.db.dao

import androidx.room.*
import org.themoviedb.celebrities.db.table.DBPerson

@Dao
interface PersonDao {

    @Query("SELECT * from person WHERE id = :id")
    suspend fun getPerson(id: Long): DBPerson

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(person: DBPerson)

}

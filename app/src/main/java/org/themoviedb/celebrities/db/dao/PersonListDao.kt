package org.themoviedb.celebrities.db.dao

import androidx.room.*
import org.themoviedb.celebrities.db.table.DBPersonList

@Dao
interface PersonListDao {

    @Query("SELECT * from persons WHERE page = :page")
    suspend fun getPersonList(page: Int): DBPersonList

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(personList: DBPersonList)
}

package org.themoviedb.celebrities.repository

import androidx.lifecycle.MutableLiveData
import org.themoviedb.celebrities.api.*
import org.themoviedb.celebrities.app.Constants
import org.themoviedb.celebrities.db.dao.*
import org.themoviedb.celebrities.db.table.*
import org.themoviedb.celebrities.model.response.*
import org.themoviedb.celebrities.util.safeLet7


class AppRepository(private val personListDao: PersonListDao?){

    private var personDao: PersonDao? = null

    constructor(personDao: PersonDao) : this(null){
        this.personDao=personDao
    }

    var persons = MutableLiveData<Resource<PersonPopularResponse>>()
    suspend fun getPersons(page: Int): Resource<PersonPopularResponse>? {

        if (persons.value != null && page == 1) return persons.value!!

        if (personListDao?.getPersonList(page)?.personList?.isNotEmpty() == true) {
            val cashedData: Resource<PersonPopularResponse> = Resource.success(
                PersonPopularResponse(
                    page.toLong() ,
                    arrayListOf(),
                    0,
                    0)
            )
            cashedData.data?.results = personListDao.getPersonList(page).personList!!
            return cashedData
        }

        val ans = apiCall { getPersonPopular(Constants.API_KEY, Constants.LANGUAGE, page) }

        if (ans.isLoading()) {
            return Resource.loading()
        } else if (ans.isSuccess() && ans.data != null) {
            val personsList = ans.data
            persons.value = Resource.success(personsList)
            return persons.value!!
        }
        return Resource.error("", null, ans.errorObject)
    }
    suspend fun insert(perosnList: DBPersonList) {
        personListDao?.insert(perosnList)
    }


    val personInfo = MutableLiveData<Resource<PersonInfoResponse>>()
    suspend fun getPersonInfo(personId: Int): Resource<PersonInfoResponse> {

        val cashPerson=personDao?.getPerson(personId.toLong())
        if (cashPerson!=null) {
            val cashedData: Resource<PersonInfoResponse> =Resource.success(
                PersonInfoResponse(personId.toLong(),"","","","","","")
            )

            safeLet7(personId.toLong(), cashPerson.name, cashPerson.biography,cashPerson.birthday,cashPerson.knownForDepartment,cashPerson.placeOfBirth,cashPerson.profilePath){
                    id,name,biography,birthday,knownForDepartment,placeOfBirth,profilePath ->
                cashedData.data= PersonInfoResponse(
                    id=id,
                    name=name,
                    biography = biography,
                    birthday = birthday,
                    knownForDepartment = knownForDepartment,
                    placeOfBirth = placeOfBirth,
                    profilePath = profilePath)
            }

            return cashedData
        }


        val ans = apiCall { getPersonInfo(personId, Constants.API_KEY, Constants.LANGUAGE) }

        if (ans.isLoading()) {
            return Resource.loading()
        } else if (ans.isSuccess()) {
            val person = ans.data
            personInfo.value = Resource.success(person)
            return personInfo.value!!
        }
        return Resource.error("", null, ans.errorObject)
    }
    suspend fun insertPerson(perosn: DBPerson) {
        personDao?.insert(perosn)
    }

}

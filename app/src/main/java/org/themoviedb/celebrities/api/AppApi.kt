package org.themoviedb.celebrities.api

import org.themoviedb.celebrities.model.response.*
import retrofit2.http.*

interface AppApi {

    @GET("person/popular")
    suspend fun getPersonPopular(
        @Query("api_key") apiKey: String,
        @Query("language") lan: String,
        @Query("page") page: Int
    ): PersonPopularResponse

    @GET("person/{person_id}")
    suspend fun getPersonInfo(
        @Path("person_id") personId:Int,
        @Query("api_key") apiKey: String,
        @Query("language") lan: String
    ): PersonInfoResponse

}
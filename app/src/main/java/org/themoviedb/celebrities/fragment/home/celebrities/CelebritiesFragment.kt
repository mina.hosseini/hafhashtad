package org.themoviedb.celebrities.fragment.home.celebrities

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.adapter.AppGenericAdapter
import org.themoviedb.celebrities.adapter.PaginationScrollListener
import org.themoviedb.celebrities.databinding.FragmentCelebritiesBinding
import org.themoviedb.celebrities.extensions.observe
import org.themoviedb.celebrities.fragment.ArchBaseFragment
import org.themoviedb.celebrities.model.response.Person
import org.themoviedb.celebrities.util.NavAnimations
import org.themoviedb.celebrities.view.cell.PersonPopularCell

@FlowPreview
@ExperimentalCoroutinesApi
class CelebritiesFragment : ArchBaseFragment<FragmentCelebritiesBinding, CelebritiesFragmentVM>() {

    override val viewModel: CelebritiesFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_celebrities


    lateinit var adapter: AppGenericAdapter

    var linearLayoutManager: LinearLayoutManager? = null
    private val PAGE_START = 1
    private var isLastPage = false
    private var TOTAL_PAGES = 0
    private var currentPage: Int = PAGE_START

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBackPressedCallback.isEnabled = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setRecyclerViewAdapterAndListener()
        loadFirstPage()
        binding.swipeRefresh.setOnRefreshListener { this.doRefresh() }
    }

    private fun setRecyclerViewAdapterAndListener() {
        binding.isEmpty = false
        binding.recyclerView.apply {
            this@CelebritiesFragment.adapter = AppGenericAdapter().apply {
                provider { ctx ->
                    PersonPopularCell(ctx) { person ->
                        navigate(
                            CelebritiesFragmentDirections.navigateCelebritiesFragmentToPersonInfoFragment(person),
                            NavAnimations.leftToRight
                        )
                    }
                }
            }
            adapter = this@CelebritiesFragment.adapter
        }

        linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.addOnScrollListener(object :
            PaginationScrollListener(linearLayoutManager!!) {
            override fun loadMoreItems() {
                isLoading = true
                currentPage += 1
                loadNextPage()
            }

            override val totalPageCount: Int
                get() = TOTAL_PAGES
            override val isLastPage: Boolean = false
            override var isLoading: Boolean = false
        })
    }

    override fun registerObservers() {
        observe(viewModel.personListResponse) {

            if (it.isLoading()) {
                if (currentPage == PAGE_START)
                    binding.loadingFirstPage = true
                else if (currentPage <= TOTAL_PAGES)
                    binding.loadingNextPage = true
                else isLastPage = true
            }

            if (it.isSuccess()) {
                val personList = it.data?.results
                if (personList != null && personList.isNotEmpty()) {
                    TOTAL_PAGES = it?.data?.totalPages!!
                    adapter.setSectionsAndNotify<PersonPopularCell, Person>(personList)

                } else {
                    binding.isEmpty = true
                }
                binding.loadingNextPage = false
                binding.loadingFirstPage = false

            }

            if (it.isError()) {
                binding.loadingNextPage = false
                binding.loadingFirstPage = false
                binding.isEmpty = true
                showSnackBar(it.errorObject?.message ?: "Unknown Error")
            }
        }
    }

    private fun doRefresh() {
        binding.loadingFirstPage = true
        binding.isEmpty = false
        adapter.sections.clear()
        adapter.notifyDataSetChanged()
        loadFirstPage()
        binding.swipeRefresh.isRefreshing = false
    }

    fun loadFirstPage() {
        currentPage = PAGE_START
        viewModel.getPersonList(currentPage)
    }

    private fun loadNextPage() {
        viewModel.getPersonList(currentPage)
    }
}
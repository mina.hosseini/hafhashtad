package org.themoviedb.celebrities.fragment

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import com.phelat.navigationresult.BundleFragment
import org.themoviedb.celebrities.extensions.*
import kotlinx.coroutines.*
import androidx.databinding.*
import androidx.lifecycle.*
import androidx.annotation.*
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.util.UiHelper
import java.util.concurrent.TimeoutException

abstract class ArchBaseFragment<B : ViewDataBinding, VM: ViewModel> : BundleFragment() ,
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @LayoutRes abstract fun layout(): Int
    abstract val viewModel: VM

    protected lateinit var binding: B

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {

        binding = DataBindingUtil.inflate( inflater, layout(), container, false )
        binding.lifecycleOwner = this
        binding.setView(this)
        binding.setVm(viewModel)
        onBindingCreated(savedInstanceState)
        registerObservers()

        return binding.root
    }

    open fun registerObservers(){}

    open fun onBindingCreated(savedInstanceState: Bundle?){}

    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }

    operator fun<T> LiveData<T>.invoke(observer: ((T)->Unit)){
        observe(this, observer)
    }

    fun showSnackBar(message: String, @ColorRes bgColor: Int? = null): Snackbar {
        return UiHelper.showSnackbar(binding.root, message, bgColor = if(bgColor == null) null else ContextCompat.getColor(requireContext(), bgColor))
    }

    private val navController: NavController?
        get() {
            return try {
                findNavController()
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

    fun navigateUp(){
        navController?.navigateUp()
    }

    protected val onBackPressedCallback = object : OnBackPressedCallback(true ) {
        override fun handleOnBackPressed() {
            findNavController().navigateUp()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, onBackPressedCallback)
    }

    open fun isNetworkConnected(): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            } else {
                TODO("VERSION.SDK_INT < M")
            }
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }

    fun fetchErrorMessage(throwable: Throwable): String? {
        var errorMsg = resources.getString(R.string.error_msg_unknown)
        if (!isNetworkConnected()) {
            errorMsg = resources.getString(R.string.error_msg_no_internet)
        } else if (throwable is TimeoutException) {
            errorMsg = resources.getString(R.string.error_msg_timeout)
        }
        return errorMsg
    }

}

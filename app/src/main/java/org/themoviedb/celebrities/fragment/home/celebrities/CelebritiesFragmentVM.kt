package org.themoviedb.celebrities.fragment.home.celebrities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import org.themoviedb.celebrities.api.Resource
import org.themoviedb.celebrities.db.dao.PersonListDao
import org.themoviedb.celebrities.db.table.DBPersonList
import org.themoviedb.celebrities.di.inject
import org.themoviedb.celebrities.model.response.PersonPopularResponse
import org.themoviedb.celebrities.mvvmutils.BaseViewModel
import org.themoviedb.celebrities.repository.AppRepository

@ExperimentalCoroutinesApi
@FlowPreview
class CelebritiesFragmentVM : BaseViewModel() {
    private val personListDao : PersonListDao by inject()
    private val repository: AppRepository = AppRepository(personListDao)

    val personListResponse = MutableLiveData<Resource<PersonPopularResponse>>()

    fun getPersonList(page:Int) = viewModelScope.launch {
        personListResponse.value = Resource.loading()
        val response = repository.getPersons(page)
        personListResponse.value = response

        if (response?.isSuccess() == true)
            insert(DBPersonList(page, response.data?.results))
    }

    fun insert(personsList: DBPersonList) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(personsList)
    }
}
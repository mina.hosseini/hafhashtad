package org.themoviedb.celebrities.fragment.home.celebrities.personInfo

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.databinding.FragmentPersonInfoBinding
import org.themoviedb.celebrities.extensions.observe
import org.themoviedb.celebrities.fragment.ArchBaseFragment


class PersonInfoFragment : ArchBaseFragment<FragmentPersonInfoBinding, PersonInfoFragmentVM>() {

    override val viewModel: PersonInfoFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_person_info
    private val args by navArgs<PersonInfoFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.person = args.person
        viewModel.getPersonInfo(args.person.id)

    }

    override fun registerObservers() {
        observe(viewModel.personInfoResponse) {

            if (it.isLoading()) {
                binding.loading=true
            }

            if (it.isSuccess()) {
                val personInfo = it.data
                if (personInfo!=null )
                    binding.personInfo=personInfo
                binding.loading=false
            }

            if (it.isError()) {
                binding.loading=false
                showSnackBar(it.errorObject?.message ?: "Unknown Error")
            }
        }
    }

}
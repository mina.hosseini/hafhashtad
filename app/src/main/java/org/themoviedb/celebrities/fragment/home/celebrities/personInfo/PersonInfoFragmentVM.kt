package org.themoviedb.celebrities.fragment.home.celebrities.personInfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import org.themoviedb.celebrities.api.Resource
import org.themoviedb.celebrities.db.dao.PersonDao
import org.themoviedb.celebrities.db.table.DBPerson
import org.themoviedb.celebrities.di.inject
import org.themoviedb.celebrities.model.response.PersonInfoResponse
import org.themoviedb.celebrities.mvvmutils.BaseViewModel
import org.themoviedb.celebrities.repository.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PersonInfoFragmentVM : BaseViewModel() {

    private val personDao: PersonDao by inject()
    private val repository: AppRepository = AppRepository(personDao)

    val personInfoResponse = MutableLiveData<Resource<PersonInfoResponse>>()

    fun getPersonInfo(id: Int) = viewModelScope.launch {
        personInfoResponse.value = Resource.loading()
        val response = repository.getPersonInfo(id)
        personInfoResponse.value = response

        if (response.isSuccess())
            insert(
                DBPerson(
                    id=id.toLong(),
                    name = response.data?.name,
                    biography = response.data?.biography,
                    birthday = response.data?.birthday,
                    knownForDepartment =response.data?.knownForDepartment,
                    placeOfBirth = response.data?.placeOfBirth,
                    profilePath = response.data?.profilePath
                )
            )
    }


    fun insert(person: DBPerson) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertPerson(person)
    }

}
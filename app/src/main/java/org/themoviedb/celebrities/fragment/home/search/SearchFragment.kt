package org.themoviedb.celebrities.fragment.home.search

import android.os.Bundle
import androidx.fragment.app.viewModels
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.databinding.FragmentSearchBinding
import org.themoviedb.celebrities.fragment.ArchBaseFragment

class SearchFragment : ArchBaseFragment<FragmentSearchBinding, SearchFragmentVM>() {

    override val viewModel: SearchFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_search

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBackPressedCallback.isEnabled = false
    }

}
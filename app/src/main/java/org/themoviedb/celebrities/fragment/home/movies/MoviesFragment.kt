package org.themoviedb.celebrities.fragment.home.movies

import android.os.Bundle
import androidx.fragment.app.viewModels
import kotlinx.coroutines.*
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.databinding.FragmentMoviesBinding
import org.themoviedb.celebrities.fragment.ArchBaseFragment

@FlowPreview
@ExperimentalCoroutinesApi
class MoviesFragment : ArchBaseFragment<FragmentMoviesBinding, MoviesFragmentVM>() {

    override val viewModel: MoviesFragmentVM by viewModels()
    override fun layout() = R.layout.fragment_movies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onBackPressedCallback.isEnabled = false
    }
}

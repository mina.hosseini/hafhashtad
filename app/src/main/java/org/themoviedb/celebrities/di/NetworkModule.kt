package org.themoviedb.celebrities.di

import android.content.Context
import com.google.gson.GsonBuilder
import org.themoviedb.celebrities.BuildConfig
import org.themoviedb.celebrities.api.AppApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.Proxy
import java.util.concurrent.TimeUnit

val networkModule = module {

    factory { GsonBuilder().create() }

    factory { provideAppApi(get()) }

}

fun provideAppApi(context: Context): AppApi {
    val baseUrl = "https://api.themoviedb.org/3/"
    val okHttpClientBuilder = OkHttpClient()
            .newBuilder()
            .proxy(Proxy.NO_PROXY)

    okHttpClientBuilder.callTimeout(30, TimeUnit.SECONDS)
    okHttpClientBuilder.readTimeout( 30, TimeUnit.SECONDS)
    okHttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        okHttpClientBuilder.addInterceptor(
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        )
    }

    val okHttpClient = okHttpClientBuilder.build()

    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AppApi::class.java)
}

inline val api : AppApi get(){
    val ans : AppApi by inject()
    return ans
}

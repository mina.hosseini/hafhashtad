package org.themoviedb.celebrities.di

import org.themoviedb.celebrities.app.App
import org.themoviedb.celebrities.db.AppDatabase
import org.koin.android.ext.koin.androidApplication
import org.koin.core.context.GlobalContext
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier
import org.koin.dsl.module

val appModule = module {
    single { androidApplication() as App }
    single { AppDatabase.getDatabase(get()).personListDao() }
    single { AppDatabase.getDatabase(get()).personDao() }
}


inline fun <reified T : Any> inject(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null
) = lazy { GlobalContext.get().koin.get<T>(qualifier, parameters) }

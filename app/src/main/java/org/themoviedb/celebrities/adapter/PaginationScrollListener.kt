package org.themoviedb.celebrities.adapter

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class PaginationScrollListener protected constructor(layoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {

    private val layoutManager: LinearLayoutManager
    abstract val totalPageCount: Int
    abstract val isLastPage: Boolean
    abstract val isLoading: Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount: Int = layoutManager.getChildCount()
        val totalItemCount: Int = layoutManager.getItemCount()
        val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0)
                loadMoreItems()
    }

    protected abstract fun loadMoreItems()

    init {
        this.layoutManager = layoutManager
    }
}
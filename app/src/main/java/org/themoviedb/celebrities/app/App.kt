package org.themoviedb.celebrities.app

import android.app.Activity
import android.app.Application
import android.os.Bundle
import org.themoviedb.celebrities.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.lang.ref.WeakReference
import java.util.*

lateinit var app: App

class App : Application(), Application.ActivityLifecycleCallbacks {

    companion object{
        private val visibleActivities = TreeSet<String>()
        val activities = LinkedHashSet<WeakReference<Activity?>>()
    }


    override fun onActivityResumed(activity: Activity) {
        removeExisting(activity)
        activities.add(WeakReference(activity))
        visibleActivities.add(activity.localClassName) }

    override fun onActivityPaused(activity: Activity) {
        visibleActivities.remove(activity.localClassName)
    }

    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
        activities.add(WeakReference(activity))
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
        removeExisting(activity)
    }

    private fun removeExisting(activity: Activity) {
        val each = activities.iterator()
        while (each.hasNext()) {
            val item = each.next()?.get()
            if (item == null || item === activity) {
                each.remove()
            }
        }
    }

    override fun onCreate() {
        super.onCreate()

        app = this
        startKoin {
            androidLogger()
            // declare used Android context
            androidContext(this@App)
            // declare modules
            modules(listOf(appModule, networkModule))
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        unregisterActivityLifecycleCallbacks(this)
    }

}

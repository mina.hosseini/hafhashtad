package org.themoviedb.celebrities.app

object Constants {
    const val API_KEY="f3d1aa684da0ff5fb2b69150dc2d4e56"
    const val ACCESS_TOKEN="eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmM2QxYWE2ODRkYTBmZjVmYjJiNjkxNTBkYzJkNGU1NiIsInN1YiI6IjYwODUyOGViNTZiOWY3MDAzZmJiOTkzMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.SEMa2EkNy7ay_-1DyZkfEehdsF-ASS0g_LE29m-Zl-g"
    const val LANGUAGE="en-US"
    const val PROFILE_PATH ="https://image.tmdb.org/t/p/original"

}
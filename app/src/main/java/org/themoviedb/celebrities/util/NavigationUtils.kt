package org.themoviedb.celebrities.util

import androidx.navigation.NavOptions
import org.themoviedb.celebrities.R

object NavAnimations{
    private val leftToRightAnim get() = NavOptions.Builder()
            .setPopEnterAnim(R.anim.box_fragment_animation_left_to_right_pop_enter)
            .setPopExitAnim(R.anim.box_fragment_animation_left_to_right_pop_exit)
            .setEnterAnim(R.anim.box_fragment_animation_right_to_left_enter)
            .setExitAnim(R.anim.box_fragment_animation_right_to_left_exit)

    val leftToRight get() = leftToRightAnim.build()


    private val bottomToTopAnim get() = NavOptions.Builder()
            .setPopEnterAnim(R.anim.box_fragment_animation_top_to_bottom_pop_enter)
            .setPopExitAnim(R.anim.box_fragment_animation_top_to_bottom_pop_exit)
            .setEnterAnim(R.anim.box_fragment_animation_bottom_to_top_enter)
            .setExitAnim(R.anim.box_fragment_animation_bottom_to_top_exit)
    val bottomToTop get() = bottomToTopAnim.build()
}

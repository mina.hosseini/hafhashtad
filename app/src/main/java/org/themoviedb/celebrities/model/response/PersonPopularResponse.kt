package org.themoviedb.celebrities.model.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PersonPopularResponse(
    val page: Long,
    var results: List<Person>,

    @SerializedName("total_pages")
    val totalPages: Int,

    @SerializedName("total_results")
    val totalResults: Long
):  Serializable


data class Person  (
    val adult: Boolean,
    val gender: Long,
    val id: Int,

    @SerializedName("known_for_department")
    val knownForDepartment: String,

    val name: String,
    val popularity: Double,

    @SerializedName("profile_path")
    val profilePath: String? = null
):  Serializable


data class KnownFor(
    val adult: Boolean? = null,

    @SerializedName("backdrop_path")
    val backdropPath: String? = null,

    @SerializedName("genre_ids")
    val genreIDS: List<Long>,

    val id: Long,

    @SerializedName("original_title")
    val originalTitle: String? = null,

    val overview: String,

    @SerializedName("poster_path")
    val posterPath: String,

    @SerializedName("release_date")
    val releaseDate: String? = null,

    val title: String? = null,
    val video: Boolean? = null,

    @SerializedName("vote_average")
    val voteAverage: Double,

    @SerializedName("vote_count")
    val voteCount: Long,

    @SerializedName("first_air_date")
    val firstAirDate: String? = null,

    val name: String? = null,

    @SerializedName("origin_country")
    val originCountry: List<String>? = null,

    @SerializedName("original_name")
    val originalName: String? = null
):  Serializable

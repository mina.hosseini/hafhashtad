package org.themoviedb.celebrities.model.response

import com.google.gson.annotations.SerializedName

class PersonInfoResponse(
    val id: Long,
    val name: String?,
    val biography: String,
    val birthday: String,

    @SerializedName( "known_for_department")
    val knownForDepartment: String,

    @SerializedName( "place_of_birth")
    val placeOfBirth: String,

    @SerializedName( "profile_path")
    val profilePath: String
){
    val knownForDepartmentFormatted get()= "Known for:\n$knownForDepartment"
    val placeOfBirthFormatted get()= "Birthplace:\n$placeOfBirth"
    val birthdayFormatted get()= "Date of Birth:\n$birthday"
}
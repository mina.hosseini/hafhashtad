@file:Suppress("unused")

package org.themoviedb.celebrities.mvvmutils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso
import org.themoviedb.celebrities.app.Constants
import org.themoviedb.celebrities.util.PixelUtil
import org.themoviedb.celebrities.util.RoundedCornersTransformation


object BindingAdapterUtils {

    @JvmStatic
    @BindingAdapter(value = ["loadFromUrl", "placeholder", "radius", "margin"], requireAll = false)
    fun setImageUrl(
        imageView: ImageView,
        url: String?,
        drawable: Drawable?,
        radius: Int = 0,
        margin: Int = 0
    ) {
        if (!url.isNullOrEmpty()) {

            val mRadius = PixelUtil.dpToPx(radius.toFloat())

            val loader = Picasso.get().load(
                if (!url.startsWith("http"))
                    Constants.PROFILE_PATH + url
                else
                    url
            )
            if (mRadius > 0)
                loader.transform(RoundedCornersTransformation(mRadius, 0))
            if (drawable != null)
                loader.placeholder(drawable)
            loader.into(imageView)
        } else {
            imageView.setImageDrawable(drawable)
        }
    }

}
package org.themoviedb.celebrities.activity.splash

import android.content.Intent
import androidx.activity.viewModels
import kotlinx.coroutines.*
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.activity.ArchBaseActivity
import org.themoviedb.celebrities.activity.main.MainActivity
import org.themoviedb.celebrities.databinding.ActivitySplashBinding

class SplashActivity :  ArchBaseActivity<ActivitySplashBinding, SplashActivityVM>() {

    override fun layout() = R.layout.activity_splash
    override val viewModel: SplashActivityVM by viewModels()

    override fun onResume() {
        super.onResume()
        CoroutineScope(Dispatchers.Main).launch {
            delay(1500)
            startActivity(Intent(this@SplashActivity,MainActivity::class.java))
        }
    }

}
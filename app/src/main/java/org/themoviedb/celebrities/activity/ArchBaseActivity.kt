package org.themoviedb.celebrities.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.databinding.*
import kotlinx.coroutines.*
import org.themoviedb.celebrities.extensions.*

abstract class ArchBaseActivity<B : ViewDataBinding, VM : ViewModel> : AppCompatActivity() ,
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    @LayoutRes abstract fun layout(): Int
    protected abstract val viewModel: VM

    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout())

        binding.lifecycleOwner = this

        binding.setView(this)
        binding.setVm(viewModel)
    }
}
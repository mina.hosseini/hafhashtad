package org.themoviedb.celebrities.activity.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import org.themoviedb.celebrities.R
import org.themoviedb.celebrities.activity.ArchBaseActivity
import org.themoviedb.celebrities.databinding.ActivityMainBinding

class MainActivity : ArchBaseActivity<ActivityMainBinding, MainActivityVM>() {

    override fun layout() = R.layout.activity_main
    override val viewModel: MainActivityVM by viewModels()

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        navController = (supportFragmentManager.findFragmentById(R.id.main_nav_host) as NavHostFragment).navController

        addMenuItems()
    }

    private fun addMenuItems() {
        binding.bottomBar.onTabSelected = {
            when (it.title) {
                "Celebrities" -> {
                        navController.navigate(
                            R.id.celebritiesFragment
                        )
                }
                "Search" -> {
                    navController.navigate(
                        R.id.searchFragment
                    )
                }
                "Movies" -> {
                    navController.navigate(
                        R.id.moviesFragment
                    )
                }

            }
        }

        binding.bottomBar.onTabReselected={
            when (it.title) {
                "Celebrities" -> {
                    if (navController.currentDestination?.id == R.id.personInfoFragment)
                        navController.navigateUp()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (navController.currentDestination?.id == R.id.personInfoFragment )
            super.onBackPressed()
    }
}